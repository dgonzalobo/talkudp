/**
 * @author dgonzalobo
 * (Desarrollado en UNIX)
 * 
 * PRACTICA 7
 * Se pide desarrollar un cliente de un servicio chat sobre protocolo UDP, para un servidor ya dado. Se indican como necesidades
 * de diseño, el que conste de dos hilos, uno para escuchar y otro para escribir en el servidor.
 * 
 */
package es.dgonzalobo;

import java.net.*;
import java.util.*;

public class TalkServidor {
	private static int PuertoServidor;
	private DatagramSocket MiSocket; 
	Hashtable<String, DatosEstacion> Estaciones;

	public TalkServidor() {  
		PuertoServidor = 7777;
		try {
			MiSocket = new DatagramSocket(PuertoServidor);
			Estaciones = new Hashtable<String, DatosEstacion>();
		} 
		catch(Exception e) { 
			System.out.println("TalkServidor error: " + e); 
		} 
	} 

	public static void main(String args[]){ 
		String texto, comando,origen, destino; 
		int pos, pos2;
		boolean salir = false;
		TalkServidor Servidor = new TalkServidor(); 
		System.out.println("\t****************************************************");
		System.out.println("\t                    TalkServer");
		System.out.println("\t                  (PUERTO: "+PuertoServidor+")");
		System.out.println("\t****************************************************\n");
		
		while(!salir) {
			try {
				byte[] BufferEntrada = new byte[1024];
				DatagramPacket PaqueteEntrada = new DatagramPacket(BufferEntrada,BufferEntrada.length); 
				
				Servidor.MiSocket.receive(PaqueteEntrada); 
				texto = new String(BufferEntrada,0,PaqueteEntrada.getLength()); 
				System.out.println("RECIBIDO de "+ PaqueteEntrada.getAddress()+": "+texto);
				pos = texto.indexOf("-");
				comando = texto.substring(0, pos);
				
				if(comando.equals("REGISTRAR")) {
					origen = texto.substring(pos+1); 
					Servidor.Registrar(origen, PaqueteEntrada.getAddress(),
							PaqueteEntrada.getPort()); 
				} 
				else if(comando.equals("LISTAR")) {
					origen = texto.substring(pos+1);
					Servidor.ListarEstaciones(origen); 
				} 
				else if(comando.equals("HABLAR")) {
					pos2 = texto.indexOf("-", pos+1); 
					origen = texto.substring(pos+1, pos2); 
					pos = texto.indexOf("-", pos2+1); 
					destino = texto.substring(pos2+1, pos); 
					texto = texto.substring(pos+1);
					Servidor.RetransmitirMensaje(texto,origen,destino);
				} 
				else if(comando.equals("SALIR")) {
					origen = texto.substring(pos+1);
					Servidor.EliminarRegistro(origen); 
				}
			} catch(Exception e){
				System.out.println ("Error en TalkServidor main: "+ e); salir= true;
			}
		}
	}

	void Registrar(String estacion, InetAddress ia, int port) { 
		String comando;
		DatosEstacion datos= new DatosEstacion(ia,port); 

		try {
			if(Estaciones.containsKey(estacion))
				comando = "ERROR"; 
			else {
				comando = "OK";
				Estaciones.put(estacion, datos);
			}

			byte[] BufferSalida = comando.getBytes();
			DatagramPacket PaqueteSalida = new DatagramPacket(BufferSalida,BufferSalida.length, datos.getIP(), datos.getPuerto());
			MiSocket.send(PaqueteSalida); 
			System.out.println("ENVIADO a "+datos.getIP()+": "+comando);
		}
		catch (NullPointerException er) {
			System.out.println("Error en Hashtable: " + er); 
		} 
		catch(Exception e) {
			System.out.println("Error en el registro: "+e); 
		}
	} 

	void EliminarRegistro(String estacion) { 
		try {
			DatosEstacion datos = (DatosEstacion) Estaciones.get(estacion); 
			String comando="TalkServidor-#FIN#";
			byte[] BufferSalida = comando.getBytes();
			DatagramPacket PaqueteSalida = new DatagramPacket(BufferSalida,BufferSalida.length, datos.getIP(), datos.getPuerto());
			MiSocket.send(PaqueteSalida);
			Estaciones.remove(estacion); 
			System.out.println("ENVIADO a "+datos.getIP()+": "+comando);
		} 
		catch(Exception e) {
			System.out.println("Error en la eliminacion del registro: "+e); 
		}
	} 

	void ListarEstaciones(String estacion) { 
		String comando;
		DatosEstacion datosDestino;

		try {
			datosDestino = (DatosEstacion) Estaciones.get(estacion); 
			Enumeration<String> lista = Estaciones.keys(); 
			while(lista.hasMoreElements()) {
				comando = "TalkServidor-"+(String) lista.nextElement();
				byte [] BufferSalida= comando.getBytes();
				DatagramPacket PaqueteSalida=new DatagramPacket(BufferSalida,
								BufferSalida.length,datosDestino.getIP(), datosDestino.getPuerto());
				MiSocket.send(PaqueteSalida);
				System.out.println("ENVIADO a "+datosDestino.getIP()+": "+comando);
			} 
		} 
		catch(Exception e) {
			System.out.println("Error en el listado de las estaciones: "+e);
		}
	} 

	void RetransmitirMensaje(String mensaje, String origen, String destino) { 
		String comando;
		DatosEstacion datos = null;
		int portdestino;
		try {
			if(Estaciones.containsKey(destino)) { 
				comando = origen + "-" + mensaje; 
				datos = (DatosEstacion) Estaciones.get(destino);
				portdestino= datos.getPuerto();
			} 
			else { 
				comando = "TalkServidor-ERROR, no existe "+destino; 
				datos = (DatosEstacion) Estaciones.get(origen);
				portdestino= datos.getPuerto(); 
			}
			byte[] BufferSalida = comando.getBytes(); 
			DatagramPacket PaqueteSalida=new DatagramPacket(BufferSalida,BufferSalida.length, datos.getIP(), portdestino);
			MiSocket.send(PaqueteSalida); 
			System.out.println("ENVIADO a "+datos.getIP()+": "+comando);
		} 
		catch(Exception e) {
			System.out.println("Error en la conversacion: " + e); 
		}
	} 
}


class DatosEstacion {
	private InetAddress DireccionIP; 
	private int Puerto;

	public DatosEstacion (InetAddress dirIP, int pto) { 
		DireccionIP= dirIP;
		Puerto= pto;
	}

	public InetAddress getIP(){
		return DireccionIP;
	}

	public int getPuerto (){ 
		return Puerto;
	}
}