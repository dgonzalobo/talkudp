/**
 * @author dgonzalobo
 * (Desarrollado en UNIX)
 * 
 * PRACTICA 7
 * Se pide desarrollar un cliente de un servicio chat sobre protocolo UDP, para un servidor ya dado. Se indican como necesidades
 * de dise�o, el que conste de dos hilos, uno para escuchar y otro para escribir en el servidor.
 * 
 * MEJORA 1
 * Se redise�a el servicio de chat para que funcione a trav�s de sesiones en salas y no indicando el usuario.
 * 
 * MEJORA 3
 * Se puede solicitar las salas abiertas en el servidor.
 * 
 * MEJORA 4
 * El menasje enviado por el cliente llegar� automaticamente a todos los usuarios de la sala donde se enceuntre, excepto el
 * como es logico.
 * 
 * MEJORA 5
 * Cuando se soliciten los usuarios al servidor, por seguridad, solo se dar�n los usuarios de la sala en la que se ha iniciado sesion
 * 
 * MEJORA 6 
 * Se modifica la clase axiliar DatosEstacionMejorado para que almacene en la hash la sala en la que se encuentre
 */

package es.dgonzalobo;

import java.io.*;
import java.net.*;

public class TalkClienteMejorado {
	private static String hostdestino= "localhost";
	private static int PuertoServidor = 7777;
	private static DatagramSocket MiSocket;
	private static TalkClienteMejorado cliente;
	private static InetAddress MaquinaServidor;
	private static String Alias, msgSend, msgRecive,sala;
	private BufferedReader Teclado;
	public boolean keepAlife, exit, listar;

	/**
	 * @funcion inicializa los flags necesarios para el correcto control de la interfaz y la vida de los hilos, abre el flujo 
	 * 			bufferReader de lectura por teclado, obtiene la direccion a la que enviar el datagrama e inicializa dicho datagrama.
	 */
	public TalkClienteMejorado(){
		try{
			keepAlife=true;
			exit=false;
			listar=false;
			Teclado = new BufferedReader(new InputStreamReader(System.in));
			MaquinaServidor = InetAddress.getByName(hostdestino); 
			MiSocket = new DatagramSocket(); 
		}
		catch(Exception e){
			System.err.println("Error en el constructor. "+e);
		}
	}

	/**
	 * @funcion asigna el argumente recibido a la variable hostdestino, crea un objeto TalkClienteMejorado que ser� usado por lo hilos
	 * 			e invoca el m�todo run, desde el que se manejar� la funcionalidad del cliente.
	 * @param args[0] hostdestino.
	 */
	public static void main(String args[]) {
		if (args.length>0) 
			hostdestino=args[0]; 
		try {
			cliente=new TalkClienteMejorado();
			cliente.run();
		} 
		catch(Exception e) {
			System.out.println("TalkClienteMejorado: ha ocurrido un error: " + e); 
		}
	}

	/**
	 * @funcion Muestra una cabecera que identifica el programa, invoca al m�todo de la clase initiaLog(), desde el que se 
	 * 			realizar� el registro del user en el server. Posteriormente se crean los hilos hablador y escuchador, ambos 
	 * 			se declaran daemon para hacerlos m�s seguros en cuanto a su tiempo de vida, se arrancan, se deja el esuchador
	 * 			como hilo principal, o hilo que decidir� sobre la vida del programa, a trav�s de la funci�n .join(). Posteriormente
	 * 			y por si el daemon fallase, se le aplica un interrupt al hilo hablador, se cierra el socket y se indica por pantalla
	 * 			el cierre del programa.
	 */
	public void run(){
		try{
			System.out.println("\t****************************************************");
			System.out.println("\t                    TalkClienteMejorado");
			System.out.println("\t     (PUERTO: "+PuertoServidor+"| Host: "+MaquinaServidor+")");
			System.out.println("\t****************************************************");
			System.out.println("\t(Para ver usuarios: 'LISTAR USUARIOS', para salas 'LISTAR SALAS')\n");

			initialLog();
			seleRoom();

			TalkHablaThreadMejorado hablador= new TalkHablaThreadMejorado(cliente);
			TalkEscuchaThreadMejorado escuchador=new TalkEscuchaThreadMejorado(cliente); 
			hablador.setDaemon(true);
			escuchador.setDaemon(true);
			hablador.start();
			escuchador.start();

			escuchador.join();
			hablador.interrupt();
			MiSocket.close();
			System.out.println("Fin de TalkClienteMejorado "+ Alias);
		}
		catch(Exception e){
			System.err.println("ERROR en run. "+e);
		}
	}

	/**
	 * @funcion pide por teclado una cadena de tal modo, que si es salir, se cerrar� el programa, si se introduce otra cadena,
	 * 			se interpretar� como el nick del usuario, y se enviar� junto con el comando "REGISTRAR" al server, para que 
	 * 			este lo a�ada a su base de usuarios, se espera la contestaci�n del servidor y si es OKEY el programa prosigue
	 * 			si es ERROR, porque ya exista se vuelve a invocar el propio m�todo initalLog para darle un nuevo intento al user.
	 * 			El env�o del comando y el nick al server, se realiza a trav�s de datagrama, almacenando el mensaje en un array
	 * 			de bits, para con ello m�s la direcci�n y el puerto de la maquina servidor crear un paquete datagrama, que ser�
	 *  		enviado a trav�s de la funcionalidad .send() de la clase DatagramSocket al server. El mismo proceso se realiza para 
	 *  		la esucha de mensajes, pero con .receive();
	 */
	public void initialLog(){
		try{
			System.out.print("Introduce tu Nick / Escriba Salir para cerrar: ");
			Alias = Teclado.readLine();
			if(Alias.equalsIgnoreCase("salir")){
				MiSocket.close();
				System.exit(0);
			}
			String comando = "REGISTRAR-" + Alias;
			byte[] BufferSalida = comando.getBytes();
			DatagramPacket PaqueteSalida = new DatagramPacket(BufferSalida,BufferSalida.length,MaquinaServidor, PuertoServidor);
			MiSocket.send(PaqueteSalida);
			System.out.println("espera confirmacion de registro del servidor"); 
			byte [] BufferEntrada = new byte[1024]; 
			DatagramPacket PaqueteEntrada = new DatagramPacket(BufferEntrada,BufferEntrada.length); 
			MiSocket.receive(PaqueteEntrada);
			comando = new String(BufferEntrada, 0, PaqueteEntrada.getLength()); 

			if(comando.equals("ERROR")){
				System.out.println("ERROR: ese nick ya existe.");
				initialLog();
			}
			else
				System.out.println("Nick registrado. \n");
		}
		catch(Exception e){
			System.err.println("Error en el registro incial. "+e);
		}

	}

	/**
	 * @funcion al igual que el metodo anterior permite anexionarse a una sala al arrancar el servicio de chat, una vez en dicha sala
	 *  		cuando se solicite los usuarios, se dar�n los de la sala, y cuando se envie un mensaje se enviara a todos los 
	 *  		participantes de dicha sala.
	 */
	public void seleRoom(){
		try{
			System.out.print("Introduce la sala / Escriba Salir para cerrar: ");
			sala = Teclado.readLine();
			if(sala.equalsIgnoreCase("salir")){
				MiSocket.close();
				System.exit(0);
			}
			String comando = "SALA-" +Alias+"-"+sala;
			byte[] BufferSalida = comando.getBytes();
			DatagramPacket PaqueteSalida = new DatagramPacket(BufferSalida,BufferSalida.length,MaquinaServidor, PuertoServidor);
			MiSocket.send(PaqueteSalida);
			System.out.println("espera confirmacion de registro del servidor"); 
			byte [] BufferEntrada = new byte[1024]; 
			DatagramPacket PaqueteEntrada = new DatagramPacket(BufferEntrada,BufferEntrada.length); 
			MiSocket.receive(PaqueteEntrada);
			comando = new String(BufferEntrada, 0, PaqueteEntrada.getLength()); 

			if(comando.equals("ERROR")){
				System.out.println("ERROR");
				initialLog();
			}
			else
				System.out.println("Has entrado en "+sala+"\n");
		}
		catch(Exception e){
			System.err.println("Error en el registro incial. "+e);
		}
	}

	/**
	 * @funcion si el mensaje anteriormente introducido por el usuario no era SALIR ni LISTAR, se muestra por pantalla un mensaje
	 * 			que pide el usuario destino ,gui�n y el mensaje que se le desea hacer llegar. Una vez conocido se invoca al m�todo iDMsg.
	 */
	public void readKeyboard(){
		try{
			if(!exit){
				if(!listar)
					System.out.print("Mensaje: ");
				msgSend=Teclado.readLine();
				iDMsg();
			}
		}
		catch(IOException e){
			System.out.println("ERROR en la lectura por teclado. "+e);
		}
	}

	/**
	 * @funcion se anidan una serie de bucles if, a fin de identificar lo que se le desea hacer llegar al server. 
	 * 			En el caso de que sea SALIR, se encadena dicho comando con el nick del user, se pone el flag exit a true y se invoca
	 * 			el m�todo sendMsg.
	 * 			Si se ha solicitado LISTAR, se encandea LISTA+nick, se activa el flag listar y se invoca al sendMsg.
	 * 			Por �ltimo si no es ninguno de los anteriores, se deduce que es un mensaje, se comprueba que este compuesto del 
	 * 			user destino y de mensaje, de no ser as� se invoca a readKeyboard para que se haga la solicitud de forma correcta. 
	 * 			En el caso de haber introducido user destino y mesnaje, se encadena de modo que respete el protocolo establecido 
	 * 			con el server y se llama a sendMsg.
	 */
	public void iDMsg(){
		if(msgSend.equalsIgnoreCase("SALIR")){
			msgSend="SALIR-"+Alias;
			exit=true;
			sendMsg();
		}
		else if(msgSend.equalsIgnoreCase("LISTAR USUARIOS")){
			msgSend="LISTAR USUARIOS-"+Alias;
			listar=true;
			sendMsg();
		}
		else if(msgSend.equalsIgnoreCase("LISTAR SALAS")){
			msgSend="LISTAR SALAS-"+Alias;
			listar=true;
			sendMsg();
		}
		else{
			msgSend="HABLAR-"+Alias+"-"+msgSend;
			sendMsg();
		}
	}

	/**
	 * @funcion transforma el mensaje a env�ar al server en eun array de bytes, crea un datagrama de salida y lo env�a a trav�s
	 * 			del m�todo send de DatagramSocket
	 */
	public void sendMsg(){
		try{
			byte[] BufferSalida = msgSend.getBytes();
			DatagramPacket PaqueteSalida = new DatagramPacket(BufferSalida,BufferSalida.length,MaquinaServidor, PuertoServidor);
			MiSocket.send(PaqueteSalida); 
		}
		catch(Exception e){
			System.err.println("ERROR en el envio. "+e);
		}
	}

	/**
	 * @funcion se reserva un array de bytes de 1024, para posteriormente crear un datagrama donde recibir lo env�ado por el server.
	 * 			Una vez recibido se creo un string con el contenido del datagrama y se invoca showMsg()
	 */
	public void readFromServer(){
		try{
			byte [] BufferEntrada = new byte[1024];
			DatagramPacket PaqueteEntrada = new DatagramPacket(BufferEntrada,BufferEntrada.length); 
			MiSocket.receive(PaqueteEntrada);
			msgRecive = new String(BufferEntrada, 0, PaqueteEntrada.getLength());
			showMsg();
		}
		catch(Exception e){
			System.err.println("ERROR en lectura server. "+e);
		}
	}

	/**
	 * @funcion Se muestra por pantalla el mensaje recibido, en el caso de que el flag lista estuviera activado, se pone a false,
	 * 			y en el caso de que lo que se reciba sea la confimarcion de la salida se pone el flag que marca la vida de los hilos
	 * 			a false.
	 */
	public void showMsg(){
		if(msgRecive.contains("ROOMS")){
			System.out.print("TalkServidor| SALAS: ");
			for(int i=1; i<(msgRecive.split("-").length);i++){
				System.out.print(msgRecive.split("-")[i]+" ; ");
			}
			System.out.print("\nMensaje: ");
		}
		else if(!msgRecive.contains("#FIN#")){
			if(listar)
				System.out.println("TalkServidor| USUARIOS: "+msgRecive.split("-")[1]);
			else
				System.out.println("\n\t"+sala+"||"+(msgRecive.split("-"))[0]+" dice: "+(msgRecive.split("-"))[1]);
			System.out.print("Mensaje: ");

		}
		else if(msgRecive.contains("#FIN#"))
			keepAlife=false;
	}
}


class TalkHablaThreadMejorado extends Thread{
	private TalkClienteMejorado cliente;

	/**
	 * @funcion recibe un objeto TalkClienteMejorado, al que apuntar� su variable cliente, a trav�s de la cual solicitar� datos 
	 * 			y m�todos de dicha clase principal.
	 * @param tc objeto TalkClienteMejorado
	 */
	public TalkHablaThreadMejorado(TalkClienteMejorado tc) {
		super("TalkerThread");
		try{
			cliente=tc;
		}
		catch(Exception e){
			System.err.println("Error en el constructor de TalkHablaThread. "+e);
		}
	}

	/**
	 * @funcion mientras el flag keepAlife sea true, solicitar� por teclado y enviar� los mensajes pertinentes al server. Para
	 * 			ello invocar� al m�todo readKeyboard de la clase TalkClienteMejorado
	 */
	public void run(){
		while(cliente.keepAlife){
			cliente.readKeyboard();
		}
	}
}


class TalkEscuchaThreadMejorado extends Thread{
	private TalkClienteMejorado cliente;

	/**
	 * @funcion recibe un objeto TalkClienteMejorado, al que apuntar� su variable cliente, a trav�s de la cual solicitar� datos 
	 * 			y m�todos de dicha clase principal.
	 * @param tc objeto TalkClienteMejorado
	 */
	public TalkEscuchaThreadMejorado(TalkClienteMejorado tc){	
		super("ListenerThread");
		try{
			cliente=tc;
		}
		catch(Exception e){
			System.err.println("ERROR en el constructor del TalkEsucuchaThread");
		}

	}

	/**
	 * @funcion mientras el flag keepAlife sea true, se leer� del server y se mostrar� por pantalla. Para
	 * 			ello invocar� al m�todo readFromServer de la clase TalkClienteMejorado.
	 */
	public void run(){
		while(cliente.keepAlife){
			cliente.readFromServer();
		}
	}
}