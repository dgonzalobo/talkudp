/**
 * @author dgonzalobo
 * (Desarrollado en UNIX)
 * 
 * PRACTICA 7
 * Se pide desarrollar un cliente de un servicio chat sobre protocolo UDP, para un servidor ya dado. Se indican como necesidades
 * de dise�o, el que conste de dos hilos, uno para escuchar y otro para escribir en el servidor.
 * 
 * MEJORA 1
 * Se redise�a el servicio de chat para que funcione a trav�s de sesiones en salas y no indicando el usuario.
 * 
 * MEJORA 3
 * Se puede solicitar las salas abiertas en el servidor.
 * 
 * MEJORA 4
 * El menasje enviado por el cliente llegar� automaticamente a todos los usuarios de la sala donde se enceuntre, excepto el
 * como es logico.
 * 
 * MEJORA 5
 * Cuando se soliciten los usuarios al servidor, por seguridad, solo se dar�n los usuarios de la sala en la que se ha iniciado sesion
 * 
 * MEJORA 6 
 * Se modifica la clase axiliar DatosEstacionMejorado para que almacene en la hash la sala en la que se encuentre
 */

package es.dgonzalobo;

import java.io.IOException;
import java.net.*;
import java.util.*;

public class TalkServidorMejorado {
	private static int PuertoServidor;
	private DatagramSocket MiSocket; 
	static Hashtable<String, DatosEstacionMejorado> Estaciones;

	public TalkServidorMejorado() {  
		PuertoServidor = 7777;
		try {
			MiSocket = new DatagramSocket(PuertoServidor);
			Estaciones = new Hashtable<String, DatosEstacionMejorado>();
		} 
		catch(Exception e) { 
			System.out.println("TalkServidor error: " + e); 
		} 
	} 

	public static void main(String args[]){ 
		String texto, comando,origen;
		int pos;
		boolean salir = false;
		TalkServidorMejorado Servidor = new TalkServidorMejorado(); 
		System.out.println("\t****************************************************");
		System.out.println("\t                    TalkServer");
		System.out.println("\t                  (PUERTO: "+PuertoServidor+")");
		System.out.println("\t****************************************************\n");

		while(!salir) {
			try {
				byte[] BufferEntrada = new byte[1024];
				DatagramPacket PaqueteEntrada = new DatagramPacket(BufferEntrada,BufferEntrada.length); 

				Servidor.MiSocket.receive(PaqueteEntrada); 
				texto = new String(BufferEntrada,0,PaqueteEntrada.getLength()); 
				System.out.println("RECIBIDO de "+ PaqueteEntrada.getAddress()+": "+texto);
				pos = texto.indexOf("-");
				comando = texto.substring(0, pos);

				if(comando.equals("REGISTRAR")) {
					origen = texto.substring(pos+1); 
					Servidor.Registrar(origen, PaqueteEntrada.getAddress(),PaqueteEntrada.getPort()); 
				} 
				else if(comando.equals("LISTAR SALAS")){
					origen = texto.substring(pos+1); 
					Servidor.listarSalas(origen);
				}
				else if(comando.equals("SALA")){
					String sala=texto.split("-")[2];
					String nick=texto.split("-")[1];
					Estaciones.get(nick).setSala(sala);
					Servidor.enviarConfirSala(nick, PaqueteEntrada.getAddress(), PaqueteEntrada.getPort());
				}
				else if(comando.equals("LISTAR USUARIOS")) {
					origen = texto.substring(pos+1);
					Servidor.ListarEstaciones(origen); 
				} 
				else if(comando.equals("HABLAR")) {
					Servidor.RetransmitirMensaje(texto.split("-")[2],texto.split("-")[1]);
				} 
				else if(comando.equals("SALIR")) {
					origen = texto.substring(pos+1);
					Servidor.EliminarRegistro(origen); 
				}
			} catch(Exception e){
				System.out.println ("Error en TalkServidor main: "+ e); salir= true;
			}
		}
	}

	public void enviarConfirSala(String estacion, InetAddress ia, int port) throws IOException{
		DatosEstacionMejorado datos= new DatosEstacionMejorado(ia,port); 
		byte[] BufferSalida = "OK".getBytes();
		DatagramPacket PaqueteSalida = new DatagramPacket(BufferSalida,BufferSalida.length, datos.getIP(), datos.getPuerto());
		MiSocket.send(PaqueteSalida); 
		System.out.println("ENVIADO a "+datos.getIP()+": "+"Sala asignada");
	}

	public void Registrar(String estacion, InetAddress ia, int port) { 
		String comando;
		DatosEstacionMejorado datos= new DatosEstacionMejorado(ia,port); 
		try {
			if(Estaciones.containsKey(estacion))
				comando = "ERROR"; 
			else {
				comando = "OK";
				Estaciones.put(estacion, datos);
			}

			byte[] BufferSalida = comando.getBytes();
			DatagramPacket PaqueteSalida = new DatagramPacket(BufferSalida,BufferSalida.length, datos.getIP(), datos.getPuerto());
			MiSocket.send(PaqueteSalida); 
			System.out.println("ENVIADO a "+datos.getIP()+": "+comando);
		}
		catch (NullPointerException er) {
			System.out.println("Error en Hashtable: " + er); 
		} 
		catch(Exception e) {
			System.out.println("Error en el registro: "+e); 
		}
	} 

	public void EliminarRegistro(String estacion) { 
		try {
			DatosEstacionMejorado datos = (DatosEstacionMejorado) Estaciones.get(estacion); 
			String comando="TalkServidor-#FIN#";
			byte[] BufferSalida = comando.getBytes();
			DatagramPacket PaqueteSalida = new DatagramPacket(BufferSalida,BufferSalida.length, datos.getIP(), datos.getPuerto());
			MiSocket.send(PaqueteSalida);
			Estaciones.remove(estacion); 
			System.out.println("ENVIADO a "+datos.getIP()+": "+comando);
		} 
		catch(Exception e) {
			System.out.println("Error en la eliminacion del registro: "+e); 
		}
	} 

	public void ListarEstaciones(String estacion) { 
		String comando;
		DatosEstacionMejorado datosDestino;

		try {
			datosDestino = (DatosEstacionMejorado) Estaciones.get(estacion); 
			Enumeration<String> lista = Estaciones.keys(); 
			while(lista.hasMoreElements()) {
				String aux=(String)lista.nextElement();
				if(Estaciones.get(aux).getSala().equals(Estaciones.get(estacion).getSala())){
					comando = "TalkServidor-"+aux;
					byte [] BufferSalida= comando.getBytes();
					DatagramPacket PaqueteSalida=new DatagramPacket(BufferSalida,
							BufferSalida.length,datosDestino.getIP(), datosDestino.getPuerto());
					MiSocket.send(PaqueteSalida);
					System.out.println("ENVIADO a "+datosDestino.getIP()+": "+comando);
				} 
			}
		} 
		catch(Exception e) {
			System.out.println("Error en el listado de las estaciones: "+e);
		}
	} 

	public void RetransmitirMensaje(String mensaje, String origen) { 
		String comando;
		DatosEstacionMejorado datos = null;
		int portdestino;
		Enumeration<String> lista = Estaciones.keys();
		try{
			while(lista.hasMoreElements()) {
				String aux=(String)lista.nextElement();
				if(Estaciones.get(aux).getSala().equals(Estaciones.get(origen).getSala())&&!aux.equals(origen)){
					comando = origen + "-" + mensaje; 
					datos = (DatosEstacionMejorado) Estaciones.get(aux);
					portdestino= datos.getPuerto();

					byte[] BufferSalida = comando.getBytes(); 
					DatagramPacket PaqueteSalida=new DatagramPacket(BufferSalida,BufferSalida.length, datos.getIP(), portdestino);
					MiSocket.send(PaqueteSalida); 
					System.out.println("ENVIADO a "+aux+": "+comando);
				}
			}
		} 
		catch(Exception e) {
			System.out.println("Error en la conversacion: " + e); 
		}
	} 

	public void listarSalas(String estacion){
		DatosEstacionMejorado datosDestino;
		String rooms=null;

		try{
			datosDestino = (DatosEstacionMejorado) Estaciones.get(estacion); 
			Enumeration<String> lista = Estaciones.keys(); 
			if(Estaciones.isEmpty()){
				byte [] BufferSalida= "No hay ninguna sala abierta".getBytes();
				DatagramPacket PaqueteSalida=new DatagramPacket(BufferSalida,
						BufferSalida.length,datosDestino.getIP(), datosDestino.getPuerto());
				MiSocket.send(PaqueteSalida);
				System.out.println("ENVIADO a "+datosDestino.getIP()+": "+"No hay ninguna sala abierta");
			}
			else{
				String aux=(String)lista.nextElement();
				rooms=Estaciones.get(aux).getSala();
				while(lista.hasMoreElements()) {
					aux=(String)lista.nextElement();
					if(!rooms.contains(Estaciones.get(aux).getSala())){
						rooms=rooms+"-"+Estaciones.get(aux).getSala();
					}	
				}
				byte [] BufferSalida= ("ROOMS-"+rooms).getBytes();
				DatagramPacket PaqueteSalida=new DatagramPacket(BufferSalida,
						BufferSalida.length,datosDestino.getIP(), datosDestino.getPuerto());
				MiSocket.send(PaqueteSalida);
				System.out.println("ENVIADO a "+datosDestino.getIP()+": "+"No hay ninguna sala abierta");
			}
		}
		catch(Exception e){
			System.err.println("ERROR en listarSalas. "+e);
		}
	}
}


class DatosEstacionMejorado {
	private InetAddress DireccionIP; 
	private int Puerto;
	private String sala;

	public DatosEstacionMejorado (InetAddress dirIP, int pto) { 
		DireccionIP= dirIP;
		Puerto= pto;
		sala=null;
	}

	public InetAddress getIP(){
		return DireccionIP;
	}

	public int getPuerto (){ 
		return Puerto;
	}

	public String getSala(){
		return sala;
	}

	public void setSala(String s){
		sala=s;
	}
}
